# Midi controller usb

### [1. liste Composants](url)

### [2. Schema circuit](url)

### [3. Code Arduino](url)

### [4. Installation des drivers](url)

### [5. Configurer Guitar Rig](url)

<hr>

## 1. Liste composants

* Arduino uno
* Foot pedal switch

## 1. Schema circuit électronique

![](Schema/schema.svg)

*Schema réalisé avec Fritzing*

## 2. Code arduino

1. Télécharger le dossier avec le code arduino [Lien](Code arduino)
2. Installer la libraries (arduino_midi_library-master [Lien GIT](https://github.com/FortySevenEffects/arduino_midi_library.git)) dans `C:\Users\Documents\Arduino\libraries` [Télécharger](https://github.com/FortySevenEffects/arduino_midi_library/archive/master.zip) 
3. Vérifier que la ligne 36 soit décommenter `MIDI_CREATE_DEFAULT_INSTANCE();`
4. Modifier les lignes avec `//*` devant
```
Ligne 47 : Nombre de bouton
Ligne 48 : Pins digital connecté
Ligne 62 : Nombre de potentiometres
Ligne 63 : Pins analogique connecté
Ligne 79 : Channel / note / CC
```



**Source :<br>**
*Vidéo : https://www.youtube.com/watch?v=6ex-j_NPVEs <br>
Fichier : https://github.com/silveirago/DIY-Midi-Controller*

## 3. Installation drivers

1. Télécharger Loop Midi : https://www.tobias-erichsen.de/software/loopmidi.html
2. Ajouter port : loopMIDI Port / 9288 / 0 Byte
3. Télécharger Hairless Midi : http://projectgus.github.io/hairless-midiserial/
4. Cocher les 2 cases > choisir l'usb Serial port > midi out loop midi Port > midi in not connected
5. Pour tester http://www.midiox.com/ 

*PS : Les 2 logiciels doit etre ouvert à chaque utilisation*

**Source :<br>**
*Vidéo : https://www.youtube.com/watch?v=DrbxkKNY-pk*

## 4. Configurer Guitar Rig